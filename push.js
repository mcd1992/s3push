#!/usr/bin/env nodejs

//
// Required modules
//
var fs            = require( "fs" )                         // Used to query the filesystem
var path          = require( "path" );                      // Used to resolve filesystem paths
var stream        = require( "stream" );                    // Used to convert string to stream
var mkdirp        = require( "mkdirp" );                    // Used to make the logging directory recursivly
var winston       = require( "winston" );                   // Used for logging
var nodemailer    = require( "nodemailer" );                // Used to send log emails
var child_process = require( "child_process" );             // Used to spawn the s3cmd proces
var exitcodes     = require( "./libs/s3cmd_exitcodes" );    // Used to determine exit status of s3cmd
var handle_status = require( "./libs/s3cmd_handlestatus" ); // Used to handle status codes returned by s3cm

//
// Try and load user config file
//
var config = undefined;
try {
	config = require( "./config" );
} catch( e ) {
	console.log( " Failed to find config.js config file" );
	console.log( "    `cp config.js.example config.js` And make changes as needed." );
	process.exit( 1 );
}

//
// Variable declarations
//
S3CMD_PATH = config ? config.S3CMD_PATH : "s3cmd";     // path / name to s3cmd
GLOBAL.SAVELOGS       = config.SAVELOGS ? true : false // Should log files be saved
GLOBAL.VERBOSE        = false;     // Should we output more information to stdout
GLOBAL.SENDLOGSEMAIL  = false;     // Should we send email logs every time we push
GLOBAL.LOGSTREAM      = undefined; // Stream object for writing logfiles / console
winston.emitErrs      = true;      // Enable winston errors

//
// Called when a error occurs, this will print the error to stdout and log it
// - level is a string to pass to winston for debug level
// - err is the text to print and log
//
function Log( level, err ) {
	if ( !LOGSTREAM ) {
		LOGSTREAM = new (winston.Logger)( {
			exitOnError: false,
			transports: [
				new (winston.transports.Console)( {
					level:            "silly",
					showLevel:        true,
					colorize:         true,
					handleExceptions: true
				} )
			]
		} );

		LOGSTREAM.on( "error", function( err ) {
			console.log( "[ERROR] Winston Error: ", err.toString() );
		} );

		if ( SAVELOGS ) {
			var logsdir = config.LOGSDIR    || "/tmp/s3push"
			var maxsize = config.MAXLOGSIZE || 10000*1024
			var maxlogs = config.MAXLOGS    || 10

			mkdirp.sync( logsdir ); // Create the logs directory

			LOGSTREAM.add( winston.transports.File, {
				level:            "silly",
				filename:         logsdir + "/output.log",
				maxsize:          maxsize,
				maxFiles:         maxlogs,
				json:             false,
				showLevel:        true,
				tailable:         true,
				handleExceptions: true
			} )
		}
	}

	LOGSTREAM.log( level, err.toString() );
}
GLOBAL.Log = Log;

//
// Make sure S3CMD_PATH is defined
//
if ( !S3CMD_PATH ) {
	Log( "error", "Your config file is malformed, make sure S3CMD_PATH is defined in the config.js" );
	process.exit( 1 );
}

//
// Test if child_process is able to find the s3cmd binary
//
if ( !child_process.spawnSync ) {
	Log( "error", "The child_process module is missing the spawnSync method." );
	Log( "error", "  Are you using a nodejs version >= 0.12 ?" );
	process.exit( 1 );
}
if ( child_process.spawnSync( S3CMD_PATH, [ "--help" ] ).error ) {
	Log( "error", "Failed to spawn " + S3CMD_PATH + ", ensure it is installed correctly." );
	process.exit( 1 );
}

//
// Run the s3cmd binary with the given parameters relative to cwd
// - arguments is an array of strings containing the parameters to pass to s3cmd
// - cwd is the current working directory to execute s3cmd in
//
// Returns the child_process.spawnSync object for s3cmd.
//
function S3CMD( arguments, cwd ) {     // Wrapper to child_process for spawning s3cmd
	var ret = child_process.spawnSync( // Spawn synchronous process to avoid callback hell
		S3CMD_PATH,      // Target program to run
		arguments || [], // s3cmd arguments
		{                // Environment parameters
			cwd: cwd || "./"
		}
	);

	return ret;
}

//
// Prints the usage / help text to the console
//
function PrintUsage( scriptname ) {
	var usage = "Usage: node " + scriptname + " <parameters> [backup dir] [target bucket]\n\n"

	usage += "Examples:\n"
	usage += "\t node push.js /home/user/importantdocs s3://bucket\n"
	// More examples
	usage += "\n"

	usage += "Parameters:\n"
	usage += "\t -h or --help:         show this help text\n"
	usage += "\t -v or --verbose:      output more information to stdout\n"
	usage += "\t -l or --email-logs    send logging emails\n"

	console.log( usage );
}

//
// Process the parameters passed via the command line
//
var scriptdir    = undefined // This will contain the directory that push.js is located in
var scriptname   = undefined // This will contain push.js or the new name if it was renamed

var targetdir    = undefined // Directory to read from
var targetbucket = undefined // S3 Bucket UNC to push backup to
var numargs      = Math.abs( process.argv.length - 2 ); // Number of arguments passed

process.argv.forEach( function( value, index, argv ) {
	if ( index == 0 ) {
		return; // node/npm binary
	}
	if ( index == 1 ) {
		// Save the cwd and program name into scriptdir / scriptname
		scriptdir  = path.dirname( value );
		scriptname = path.basename( value );

		if ( numargs < 1 ) { // No parameters passed to program
			PrintUsage( scriptname );
			process.exit( 0 );
		}

		return;
	}

	// index 0 and 1 are always defined
	var argnum = index - 1; // Create a variable that contains which argument we're on

	// Process parameters
	switch ( value ) {
		case "-h":
		case "--help":
			PrintUsage( scriptname );
			process.exit( 0 );
			break;

		case "-v":
		case "--verbose":
			GLOBAL.VERBOSE = true;
			break;

		case "-l":
		case "--email-logs":
			GLOBAL.SENDLOGSEMAIL = true;
			break;

		default:
			if ( argnum == (numargs-1) ) {   // Target directory
				targetdir = value;
				break;
			} else if ( argnum == numargs ) { // Target s3 bucket
				targetbucket = value;
				break;
			}

			Log( "error", "Unsupported parameter: " + value + "\n\n" );
			//PrintUsage( scriptname );
			process.exit( 1 );
			break;
	}
} );

//
// Make sure the target dir and bucket UNC were passed as the last parameters
//
if ( !targetdir || !targetbucket ) {
	Log( "error", "You must specify the target directory to read from," );
	Log( "error", "  and the target bucket to backup to as the last two parameters.\n" );
	Log( "error", "  Use `node " + scriptname + " -h` to view the help text" );
	process.exit( 1 );
}

//
// Setup email transporter to send logging emails
//
var email_transport = undefined; // This will contain the nodemailer transport if the settings are correct
if ( SENDLOGSEMAIL ) {
	if ( !config || (!config.EMAILUSER || !config.EMAILPASS || !config.EMAILTARGET || !config.EMAILSUBJECT) ) {
		Log( "error", "Can't send log emails without EMAILUSER, EMAILPASS and EMAILTARGET defined in the config file." );
		Log( "error", "  Copy the config.js.example to config.js if needed and define the EMAIL* settings." );
		process.exit( 1 );
	}

	email_transport = nodemailer.createTransport( {
		service: "Gmail",
		auth: {
			user: config.EMAILUSER,
			pass: config.EMAILPASS
		}
	} );
}

//
// Validate the config file
//
var cmd = S3CMD( [ "--dry-run" ] ); // Run s3cmd with no actions
handle_status( cmd.status, cmd );   // Handle the exit code if it encountered any errors

//
// Check if the target folder exists and is valid
//
try {
	var folder = fs.lstatSync( targetdir );

	if ( !folder.isDirectory() ) {
		Log( "error", "`" + targetdir + "` is not a folder" );
		process.exit( 1 );
	}
} catch( e ) {
	Log( "error", "`" + targetdir + "` does not exist or is not a valid folder" );
	process.exit( 1 );
}

//
// Check if the bucket passed by command line is valid
//
var bucket = targetbucket.match( /[Ss]3:\/\/[^\/]+/ ); // Match the S3 UNC without the suffix / target folder
if ( bucket && bucket[0] ) { // We have a valid s3 bucket UNC
	var cmd    = S3CMD( [ "info", bucket[0] ] );
	var status = handle_status( cmd.status, cmd ); // Handle any errors

	if ( status != "EX_OK" ) { // Check if s3cmd returned an error. If not the bucket UNC is valid and accessible
		Log( "error", "The given UNC `" + targetbucket + "` is invalid" );
		Log( "error", "  Make sure the UNC is correct and you have read/write permissions to the bucket. [" + status + "]\n" );
		process.exit( 1 );
	}

} else { // Malformed s3 bucket UNC given
	Log( "error", "The bucket UNC `" + targetbucket + "` is not a valid S3 UNC" );
	process.exit( 1 );
}

//
// All arguments are processed correctly and the config files are valid.
//

//
// Send a 'push started email' to test the email connection and generate a message-id
//
var email_message_id  = undefined;        // Message ID to use for future emails, used for grouping.
var email_date_string = Date().toString() // Date string for the log emails
if ( SENDLOGSEMAIL ) {
	var text = "Push started at " + email_date_string + "\n"
	text    += "Pushing `" + targetdir + "` to `" + targetbucket + "`\n"

	email_transport.sendMail( {
			from:    config.EMAILUSER,
			to:      config.EMAILTARGET,
			subject: config.EMAILSUBJECT + " [" + email_date_string + "]",
			text:    text
		},
		function(err, info) {
			if ( err ) {
				Log( "error", "Failed to send email log, check config.js EMAIL* settings" );
				Log( "error", err.toString() );
				process.exit( 1 );
			}
			email_message_id = info.messageId
		}
	);
}

//
// Setup s3cmd parameters
//
var s3cmd_params = [];

// Insert user defined parameters at beginning
if (config.S3PARAMS) {
	var params = config.S3PARAMS;
	for (var index in params) {
		s3cmd_params.push(params[index]);
	}
}

// Append s3cmd params for syncing without deletion
var push_params = [
	"--no-delete-removed", "--recursive",
	"sync", targetdir, targetbucket
];
for (var index in push_params) {
	s3cmd_params.push(push_params[index]);
}

//
// Start pushing the data now with s3cmd
//
var push = child_process.spawn( S3CMD_PATH, s3cmd_params );

//
// Setup event handlers
//
push.on( "error", function( err ) { // Error event handler
	Log( "error", "[S3CMD ERROR] " + err.toString() );
} );

var push_stdall = "" // This will contain both stdout and stderr text
var push_stdout = "" // This will contain all the data that was pushed to stdout
push.stdout.on( "data", function( data ) {
	data = data.toString();

	if (data.match(/^INFO: \[\d+\/\d+\]/)) { // Ignore progress indicators
		console.log(data);
		return;
	}
	push_stdout += data;
	push_stdall += data;

	if ( VERBOSE ) {
		Log( "info", "[S3CMD STDOUT] " + data );
	}
} );

var push_stderr = "" // This will contain all the data that was pushed to stderr
push.stderr.on( "data", function( data ) {
	data = data.toString();

	if (data.match(/^INFO: \[\d+\/\d+\]/)) { // Ignore progress indicators
		console.log(data);
		return;
	}
	push_stderr += data;
	push_stdall += data;

	if ( VERBOSE ) {
		Log( "info", "[S3CMD STDERR] " + data );
	}
} );

push.on( "exit", function( status, signal ) {
	//
	// Push completed, with or without errors
	//
	var push_errored = false;
	var exit_status = handle_status( status, push );
	if ( status > 9 ) { // Exit codes < 10 are successful, but might have extra metadata
		Log( "error", "s3cmd returned an error during push, check logs. [" + exit_status + "]\n" );
		Log( "error", push_stdall );
		push_errored = true;
	}

	//
	// Send email log if parameter specified
	//
	if ( SENDLOGSEMAIL ) {
		var text = push_errored ? "[ERROR] S3CMD RETURNED AN ERROR ON EXIT! Check attached log file for more info.\n\n" : "";
		text    += "Push finished at " + Date().toString() + "\n";

		push_stdall += "\nExit Code: " + status + " [" + exit_status + "]\n";

		email_transport.sendMail( {
				from:        config.EMAILUSER,
				to:          config.EMAILTARGET,
				subject:     config.EMAILSUBJECT + (push_errored ? " [ERROR]" : "") + " [" + email_date_string + "]",
				inReplyTo:   email_message_id,
				text:        text,
				attachments: [ {
					filename: "log.txt",
					content:  push_stdall
				} ]
			},
			function(err, info) {
				if ( err ) {
					Log( "error", "Failed to send email log, check config.js EMAIL* settings" );
					Log( "error", err );
					process.exit( 1 );
				}
			}
		);
	}

	if ( VERBOSE ) {
		Log( "info", "Push finished.\n\n\n" );
	}

} );

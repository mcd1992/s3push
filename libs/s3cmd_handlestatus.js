var exitcodes = require( "./s3cmd_exitcodes" );

function _HandleConfigError() {
	Log( "error", "Your s3cmd config file is malformed, use `s3cmd --configure` to repair / create it." );
	process.exit( 1 );
}

function _Handle404Error() {
	Log( "error", "The given bucket was not found. Check the UNC and make sure the bucket exists." );
	process.exit( 1 );
}

module.exports = function( status, process ) {
	switch ( exitcodes[status] ) {
		case "EX_CONFIG":
			_HandleConfigError();
			return "EX_CONFIG";

		case "EX_NOTFOUND":
			_Handle404Error();
			return "EX_NOTFOUND";

		default:
			//console.log( "Default handle: " + exitcodes[status] );
			return exitcodes[status];
	}
}

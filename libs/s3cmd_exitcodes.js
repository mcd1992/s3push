// https://github.com/s3tools/s3cmd/blob/master/S3/ExitCodes.py
module.exports = {
	0:   "EX_OK",
	1:   "EX_GENERAL",
	2:   "EX_PARTIAL",       // some parts of the command succeeded, while others failed
	10:  "EX_SERVERMOVED",   // 301: Moved permanantly & 307: Moved temp
	11:  "EX_SERVERERROR",   // 400, 405, 411, 416, 501: Bad request, 504: Gateway Time-out
	12:  "EX_NOTFOUND",      // 404: Not found
	13:  "EX_CONFLICT",      // 409: Conflict (ex: bucket error)
	14:  "EX_PRECONDITION",  // 412: Precondition failed
	15:  "EX_SERVICE",       // 503: Service not available or slow down
	64:  "EX_USAGE",         // The command was used incorrectly (e.g. bad command line syntax)
	70:  "EX_SOFTWARE",      // internal software error (e.g. S3 error of unknown specificity)
	71:  "EX_OSERR",         // system error (e.g. out of memory)
	72:  "EX_OSFILE",        // OS error (e.g. invalid Python version)
	74:  "EX_IOERR",         // An error occurred while doing I/O on some file.
	75:  "EX_TEMPFAIL",      // temporary failure (S3DownloadError or similar, retry later)
	77:  "EX_ACCESSDENIED",  // Insufficient permissions to perform the operation on S3
	78:  "EX_CONFIG",        // Configuration file error

	128: "_EX_SIGNAL",
	//2:   "_EX_SIGINT",
	130: "EX_BREAK"          // Control-C (KeyboardInterrupt raised) (EX_SIGNAL + EX_SIGINT)
};

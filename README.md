s3push
======
**About:**
This is a tool wrapping around the s3cmd tool that allows pushing changes to a remote s3 bucket without making any changes to the host machine. This is useful for using Amazon's s3 buckets as an off-site backup service. Read the config.js.example file for more info.


**Quick Start:**
1. Install git if needed and clone this repository.
2. Install nodejs, npm, and s3cmd.
  * (Requires NodeJS version >= 0.12)
  * (Requires s3cmd version >= 1.5.2)
3. Run `s3cmd --configure`, as the auto-executing user, to generate a config file for the user.
4. Cd into the s3push directory that was cloned above. Run `npm install`.
5. Copy the `config.js.example` to `config.js` in the same directory and makes changes as needed.
6. Use `nodejs push.js --help` for more info.
